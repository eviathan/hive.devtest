import { useState, useEffect, Dispatch } from "react"

export default function useDebouncedState<TValue>(
  initialValue: TValue,
  delay: number = 500,
): [TValue, TValue, Dispatch<TValue>] {
  const [value, setValue] = useState<TValue>(initialValue)
  const [debouncedValue, setDebouncedValue] = useState<TValue>(initialValue)
  useEffect(() => {
    const handler = setTimeout(() => {
      setDebouncedValue(value)
    }, delay)
    return () => {
      clearTimeout(handler)
    }
  }, [value, delay])
  return [debouncedValue, value, setValue]
}
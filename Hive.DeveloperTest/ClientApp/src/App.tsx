import * as React from 'react';
import { Route } from 'react-router';
import Layout from './components/Layout';
import Homepage from './containers/Homepage';

import "./app.scss";

export default () => (
  <Layout>
    <Route exact path='/' component={Homepage} />
  </Layout>
);

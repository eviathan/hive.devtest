import React, { useState } from 'react';
import { Location } from '../store/Locations';
import GoogleMapReact from 'google-map-react';

import "./map.scss";

interface Coords {
  lat: number,
  lng: number
}

interface Props {
  locations: Array<Location>,
  center: Coords,
  zoom: number
}

interface MapMarkerProps {
  location: Location
}

function Map({ locations, center, zoom }: Props) {
  return (
    <div className="map">
      <GoogleMapReact
        bootstrapURLKeys={{ key: "" }}
        defaultCenter={center}
        defaultZoom={zoom}>
        {locations
          ? locations.map((location, i) =>
            <MapMarker
              key={i}
              location={location}
              lat={location.latitude}
              lng={location.longitude}
            />)
          : null
        }
      </GoogleMapReact>
    </div >
  );
}

function MapMarker({ location }: MapMarkerProps & Coords) {
  return (
    <i className="map-marker">{location.id}</i>
  );
}

export default Map;
import * as React from 'react';
import "./nav.scss";

function Nav() {
  return (
    <header>
      <div className="wrapper">
        <img className="logo" src="https://hivemarketingcloud.com//media/gn0l0tnc/hive_2015_logo_lockup_aw_rgb.svg" width="138" alt="" />
      </div>
    </header>
  );
}

export default Nav;
import * as React from 'react';
import { Container } from 'reactstrap';

import Nav from './Nav';
import Map from './Map';

import "./layout.scss";

export default class Layout extends React.PureComponent<{}, { children?: React.ReactNode }> {
  public render() {
    return (
      <>
        <Nav />
        <main>
          {this.props.children}
        </main>
      </>
    );
  }
}
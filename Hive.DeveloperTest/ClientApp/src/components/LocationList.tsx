
import React, { useState, useEffect } from 'react';
import useDebouncedState from '../hooks/UseDebouncedState';
import { Location } from '../store/Locations';

import "./locationlist.scss";

interface Props {
  locations: Array<Location>,
  onDidSearch(query: string): void
  onDidClickLocation(id: number): void
}

function LocationList({ locations, onDidSearch, onDidClickLocation }: Props) {

  const [debouncedSearchTerm, searchTerm, setSearchTerm] = useDebouncedState("", 300);

  useEffect(() => {
    onDidSearch(searchTerm);
  }, [debouncedSearchTerm]);

  return (
    <div className="location-list">
      <div className="input-wrapper">
        <input
          id="search"
          type="search"
          placeholder="Search..."
          onChange={(event) => setSearchTerm(event.target.value)}
          autoFocus required />
        <button type="submit" onClick={() => onDidSearch(searchTerm)}>Go</button>
      </div>
      <div className="items">
        <ul>
          {
            locations
              ? locations.map((location, i) =>
                <li key={i}>
                  <LocationListItem location={location} onDidClick={onDidClickLocation} />
                </li>)
              : null
          }
          </ul>
      </div>
    </div>
  );
}

interface LocationItemProps {
  location: Location,
  onDidClick(id: number): void
}

function LocationListItem({ location, onDidClick }: LocationItemProps) {
  return (
    <div className="location-list__item" onClick={() => onDidClick(location.id)}>
      <p className="id">{location.id}</p>
      <div className="details">
        {location.description ? <h3>{location.description}</h3> : null}
        <p>{location.postcode}</p>
      </div>
    </div>
  );
}
      
export default LocationList;
import * as Locations from './Locations';


export interface ApplicationState {
  locations: Locations.LocationsState | undefined;
}

export const reducers = {
  locations: Locations.reducer
};

export interface AppThunkAction<TAction> {
  (dispatch: (action: TAction) => void, getState: () => ApplicationState): void;
}

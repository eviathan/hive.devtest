import { Action, Reducer } from 'redux';
import { AppThunkAction } from './';

// -----------------
// STATE 
// ----------------
export interface LocationsState {
  isLoading: boolean;
  locations: Array<Location>;
}

export interface Location {
  id: number;
  latitude: number;
  longitude: number;
  postcode: string;
  description: string;
}

// -----------------
// ACTIONS
// ----------------
interface RequestSearchLocationsAction {
  type: 'REQUEST_SEARCH_LOCATIONS';
  query: string;
}

interface RequestAllLocationsAction {
  type: 'REQUEST_ALL_LOCATIONS';
}

interface ReceiveLocationsAction {
  type: 'RECEIVE_LOCATIONS';
  locations: Array<Location>;
}

type KnownAction = RequestSearchLocationsAction | RequestAllLocationsAction | ReceiveLocationsAction;

// ----------------
// ACTION CREATORS
// ----------------
export const actionCreators = {
  requestAllLocations: (): AppThunkAction<KnownAction> => (dispatch, getState) => {

    const appState = getState();

    if (appState && appState.locations) {
      fetch('map/locations')
        .then(response => response.json() as Promise<Array<Location>>)
        .then(data => {
          dispatch({
            type: 'RECEIVE_LOCATIONS', locations: data
          });
        });

      dispatch({ type: 'REQUEST_ALL_LOCATIONS' });
    }
  },
  requestSearchLocations: (query: string): AppThunkAction<KnownAction> => (dispatch, getState) => {

    const appState = getState();

    if (appState && appState.locations) {
      fetch(`map/search?query=${query}`)
        .then(response => response.json() as Promise<Array<Location>>)
        .then(data => {
          dispatch({ type: 'RECEIVE_LOCATIONS', locations: data });
        });

      dispatch({ type: 'REQUEST_SEARCH_LOCATIONS', query: query });
    }
  }
};

// ----------------
// REDUCER
// ----------------
const unloadedState: LocationsState = { locations: [], isLoading: false };

export const reducer: Reducer<LocationsState> = (state: LocationsState | undefined, incomingAction: Action): LocationsState => {

  if (state === undefined) {
    return unloadedState;
  }

  const action = incomingAction as KnownAction;

  switch (action.type) {
    case 'REQUEST_SEARCH_LOCATIONS':
      return {
        locations: state.locations,
        isLoading: true
      };
    case 'REQUEST_ALL_LOCATIONS':
      return {
        locations: state.locations,
        isLoading: true
      };
    case 'RECEIVE_LOCATIONS':
      return {
        locations: action.locations,
        isLoading: false
      };
  }

  return state;
};

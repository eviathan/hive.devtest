import * as React from 'react';
import { connect } from 'react-redux';
import { RouteComponentProps } from 'react-router';
import { Link } from 'react-router-dom';
import { ApplicationState } from '../store';
import * as LocationsStore from '../store/Locations';
import Fuse from 'fuse.js'

import Nav from '../components/Nav';
import Map from '../components/Map';
import LocationList from '../components/LocationList';

type Props =
  LocationsStore.LocationsState
  & typeof LocationsStore.actionCreators
  & RouteComponentProps

class Homepage extends React.PureComponent<Props> {

  public componentDidMount() {
    this.props.requestSearchLocations("")
  }

  private handleSearch(query: string = "") {
    this.props.requestSearchLocations(query);
  }

  private handleClickLocation(id: number) {
    console.log(id);
  }

  public render() {
    return (
      <>
        <Map
          locations={this.props.locations}
          center={{
            lat: 51.5433398744076,
            lng: -2.57151281453591
          }}
          zoom={15  }
        />
        <LocationList
          locations={this.props.locations}
          onDidSearch={(query) => this.handleSearch(query)}
          onDidClickLocation={() => this.handleClickLocation}
        />
      </>
    );
  }
}

export default connect(
  (state: ApplicationState) =>
    state.locations, LocationsStore.actionCreators
)(Homepage as any);

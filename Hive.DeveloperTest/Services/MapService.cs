using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Hive.DeveloperTest.Models;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;
using NinjaNye.SearchExtensions;

namespace Hive.DeveloperTest.Services
{
  public class MapService : IMapService
  {
    private readonly ILogger<MapService> _logger;
    private readonly Configuration _config;

    private IEnumerable<Location> Locations { get; }

    public MapService(ILogger<MapService> logger, IOptions<Configuration> config)
    {
      _logger = logger;
      _config = config.Value;

      Locations = LoadData();
    }
      
    private IEnumerable<Location> LoadData()
    {
      try
      {
        using (var sr = new StreamReader(_config.LocationDataPath))
        {
          var locationDataString = sr.ReadToEnd();
          return JsonConvert.DeserializeObject<IEnumerable<Location>>(locationDataString);
        }
      }
      catch (Exception ex)
      {
        _logger.LogError(ex, ex.Message);
        throw;
      }
    }

    public async Task<IEnumerable<Location>> GetAllLocations()
    {
      return Locations;
    }

    public async Task<IEnumerable<Location>> SearchLocations(string query)
    {
      return Locations
        .Search(x => x.Postcode, x => x.Description)
        .SetCulture(StringComparison.OrdinalIgnoreCase)
        .Containing(query)
        .Take(_config.MapSearchResultSize);
    }
  }
}

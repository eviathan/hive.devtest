using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Hive.DeveloperTest.Models;

namespace Hive.DeveloperTest.Services
{
  public interface IMapService
  {
    Task<IEnumerable<Location>> GetAllLocations();

    Task<IEnumerable<Location>> SearchLocations(string query);
  }
}

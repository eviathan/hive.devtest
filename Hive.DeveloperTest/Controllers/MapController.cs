﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using Hive.DeveloperTest.Exceptions;
using Hive.DeveloperTest.Models;
using Hive.DeveloperTest.Services;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace Hive.DeveloperTest.Controllers
{
  [ApiController]
  [Route("[controller]")]
  public class MapController : ControllerBase
  {
    private readonly ILogger<MapController> _logger;
    private readonly IMapService _mapService;

    public MapController(ILogger<MapController> logger, IMapService mapService)
    {
      _logger = logger;
      _mapService = mapService;
    }

    [HttpGet("Locations")]
    public async Task<IEnumerable<Location>> GetAllLocations()
    {
      try
      {
        return await _mapService.GetAllLocations();
      }
      catch (Exception ex)
      {
        _logger.LogError(ex, ex.Message);
        throw new HttpResponseException(HttpStatusCode.InternalServerError);
      }
    }

    [HttpGet("Search")]
    public async Task<IEnumerable<Location>> Search([FromQuery]MapSearchRequest request)
    {
      if(!ModelState.IsValid)
        throw new HttpResponseException(HttpStatusCode.BadRequest);

      try
      {
        return await _mapService.SearchLocations(request.Query);
      }
      catch (Exception ex)
      {
        _logger.LogError(ex, ex.Message);
        throw new HttpResponseException(HttpStatusCode.InternalServerError);
      }
    }
  }
}

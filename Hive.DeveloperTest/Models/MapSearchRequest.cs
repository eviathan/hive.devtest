using System;
using System.ComponentModel.DataAnnotations;

namespace Hive.DeveloperTest.Models
{
  public class MapSearchRequest
  {
    [RegularExpression("^[a-zA-Z0-9 ]*$")]
    public string Query { get; set; }
  }
}

using System;
namespace Hive.DeveloperTest.Models
{
  public class Location
  {
    public int Id { get; set; }

    public decimal Latitude { get; set; }

    public decimal Longitude { get; set; }

    public string Postcode { get; set; }

    public string Description { get; set; }
  }
}

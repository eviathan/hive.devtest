using System;
namespace Hive.DeveloperTest
{
  public class Configuration
  {
    public string LocationDataPath { get; set; }

    public int MapSearchResultSize { get; set; }
  }
}

using System;
using System.Net;

namespace Hive.DeveloperTest.Exceptions
{
  public class HttpResponseException : Exception
  {
    public HttpResponseException(HttpStatusCode status)
    {
      Status = status;
    }

    public HttpStatusCode Status { get; set; } = HttpStatusCode.InternalServerError;

    public object Value { get; set; }
  }
}
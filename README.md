# Hive Developer Test 2020

![](https://i.imgur.com/IoHKi82.png)

## Installation

Change Directory to `ClientApp` directory run `npm i` i.e.

`cd Hive.DeveloperTest/ClientApp && npm i`

## Usage

Open the solloution then build and Run (tested in VS for Mac)

Website:
https://localhost:5001/

API Docs:
https://localhost:5001/swagger/
